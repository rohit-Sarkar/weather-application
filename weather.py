from tkinter import *
import tkinter as tk #imports Tkinter in namespace but renames it locally to tk to save typing
from geopy.geocoders import Nominatim #Nominatim returns a location point by address
from tkinter import ttk, messagebox #ttk provides modern themed widget set& API
from timezonefinder import TimezoneFinder
from datetime import datetime
import requests #to send HTTP requests using Python
import pytz 

root = Tk()
root.title("Weather App")
root.geometry("900x500+300+200") 
root.resizable(False, False)

#for the search box
Search_image = PhotoImage(file="searchBox.png")
myImage = Label(image=Search_image)
myImage.place(x=20, y=20)

textfield = tk.Entry(root, justify="center", width=17, font=("poppins",25,"bold"), bg="#404040",border=0,fg="white")
textfield.place(x=50, y=40)
textfield.focus()

Search_icon = PhotoImage(file="search_icon.png")
myImage_icon = Button(image=Search_icon, borderwidth=0,cursor="hand2",bg="#404040", border=0, fg="white")
myImage_icon.place(x=400, y=34)

#for logo
logo_image = PhotoImage(file="logo.png")
logo = Label(image=logo_image)
logo.place(x=150, y=100)

#for bottom box
Frame_image = PhotoImage(file="box.png")
frame_myImage = Label(image=Frame_image)
frame_myImage.pack(padx=5,pady=5,side=BOTTOM)

#label
label1 = Label(root, text="Wind", font=("Helvetica",15,'bold'),fg="white",bg="#1ab5ef")
label1.place(x=120, y=400)

label2 =  Label(root, text="Humidity", font=("Helvetica",15,'bold'),fg="white",bg="#1ab5ef")
label2.place(x=250, y=400)

label3 = Label(root, text="Description", font=("Helvetica",15,'bold'),fg="white",bg="#1ab5ef")
label3.place(x=430, y=400)

label4 = Label(root, text="Pressure", font=("Helvetica",15,'bold'),fg="white",bg="#1ab5ef")
label4.place(x=650, y=400)

t=Label(font=("arial",70,'bold'), fg="#ee666d")
t.place(x=400,y=150)
c = Label(font=("arial",15,'bold'))
c.place(x=400, y=250)

root.mainloop() #method in the main window that executes what we wish to exexute in the app

